﻿namespace CodingDojo09
{
    public class Ant
    {
        public Ant()
        {
            Position = new Coordinates(0, 0);
            Direction = Direction.Up;
        }

        public Coordinates Position { get; set; }

        public Direction Direction { get; set; }
    }
}