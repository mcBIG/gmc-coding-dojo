﻿using NUnit.Framework;

namespace CodingDojo09
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            _ant = new Ant();
        }

        private Ant _ant;

        [Test]
        public void Ant_Created_IsAtCoordinatesZeroZero()
        {
            Assert.AreEqual(new Coordinates(0, 0), _ant.Position);
        }

        [Test]
        public void Ant_Created_IsDirectionUp()
        {
            Assert.AreEqual(Direction.Up, _ant.Direction);
        }

        [TestCase(Direction.Left, Direction.Up)]
        [TestCase(Direction.Up, Direction.Right)]
        [TestCase(Direction.Right, Direction.Down)]
        [TestCase(Direction.Down, Direction.Left)]
        public void GetNextDirection_AntDirectionOnColor_AntNewHeading(Direction inputDirection, Direction outputDirection)
        {
            _ant.Direction = inputDirection;
            var direction = AntNavigator.GetNextDirection(_ant, Color.White);
            Assert.AreEqual(outputDirection, direction);
        }
    }
}