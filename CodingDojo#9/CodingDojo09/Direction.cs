﻿namespace CodingDojo09
{
    public enum Direction
    {
        Down,
        Up,
        Right,
        Left
    }
}