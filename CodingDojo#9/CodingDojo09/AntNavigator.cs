namespace CodingDojo09
{
    public static class AntNavigator
    {
        public static Direction GetNextDirection(Ant ant, Color white)
        {
            switch (ant.Direction)
            {
                case Direction.Right: return Direction.Down;
                case Direction.Up: return Direction.Right;
                case Direction.Down: return Direction.Left;
                default: return Direction.Up;
            }
        }
    }
}