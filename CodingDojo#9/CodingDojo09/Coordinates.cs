﻿namespace CodingDojo09
{
    public class Coordinates
    {
        public Coordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public override bool Equals(object obj)
        {
            var cords = (Coordinates) obj;
            return X == cords.X && Y == cords.Y;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}