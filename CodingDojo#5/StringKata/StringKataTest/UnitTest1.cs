﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringKata;

namespace StringKataTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Add_InputIsEmpty_ReturnZero()
        {
            var result = Program.Add(string.Empty);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Add_InputIsOne_ReturnOne()
        {
            var result = Program.Add("1");
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void Add_InputIsTwo_ReturnTwo()
        {
            var result = Program.Add("2");
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void Add_InputIsFortyTwo_ReturnFortyTwo()
        {
            var result = Program.Add("42");
            Assert.AreEqual(42, result);
        }

        [TestMethod]
        public void Add_InputIsOneAndTwo_ReturnThree()
        {
            var result = Program.Add("1,2");
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void Add_InputIsTwoAndThree_ReturnsFive()
        {
            var result = Program.Add("2,3");
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void Add_InputIsTwoThreeAndFour_ReturnsNine()
        {
            var result = Program.Add("2,3,4");
            Assert.AreEqual(9, result);
        }

        [TestMethod]
        public void Add_InputIsThreeAndFourContainsNewLine_ReturnsNine()
        {
            var result = Program.Add("2\n3,4");
            Assert.AreEqual(9, result);
        }

        [TestMethod]
        public void Add_InputIsCotainsCustomDelimiter()
        {
            var result = Program.Add("//;\n1;2");
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Add_NegativeNumberInInputThrowsException()
        {
            var result = Program.Add("-1");
        }

        [TestMethod]
        public void Add_InputContainsMinusOne_MinusOneIsInExceptionMessage()
        {
            try
            {
                var result = Program.Add("-1");
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("-1"));
            }   
        }

        [TestMethod]
        public void Add_InputContainsMinusOneAndMinusTwo_ExceptionContainsAllNegatives()
        {
            try
            {
                var result = Program.Add("-1,-2");
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("-1,-2"));
            }
        }

        [TestMethod]
        public void Add_InputIsOneThreeThousandOne_ReturnsFour() {
            var result = Program.Add("1,3,1001");
            Assert.AreEqual(4, result);
        }
    }
}
