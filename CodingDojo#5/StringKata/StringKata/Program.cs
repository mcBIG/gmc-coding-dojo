﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringKata
{
    public class Program
    {
        static void Main(string[] args)
        {

        }

        public static int Add(string expression)
        {

            if (string.IsNullOrEmpty(expression))
                return 0;

            var splitters = GetSplitters(ref expression);
            return GetSum(expression, splitters);
        }

        private static int GetSum(string expression, char[] splitters)
        {
            var splitedNumber = expression.Split(splitters);
            var parsedNumbers = splitedNumber.Select(item => int.Parse(item));
            var negativeNumbers = parsedNumbers.Where(number => number < 0);
            if (negativeNumbers.Count() > 0) 
                throw new Exception("negatives not allowed - " + String.Join(",",negativeNumbers));
           return parsedNumbers.Sum(item => (item <= 1000) ? item : 0);
        }

        private static char[] GetSplitters(ref string expression)
        {
            var splitters = new char[] { ',', '\n' };

            if (expression.StartsWith("//"))
            {
                splitters = new char[] { expression[2] };
                expression = expression.Substring(4);
            }
            return splitters;
        }
    }
}
