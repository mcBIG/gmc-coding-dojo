﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenisKataTest
{
    public class Game
    {
        int player1Score = 0;
        int player2Score = 0;

        Dictionary<int, string> ScoreStrings = new Dictionary<int, string>(){
            {0, "Love"},
            {1, "Fifteen"},
            {2, "Thirty"},
            {3, "Fourty"}
        };

        public string GetScore()
        {
            if (player1Score == player2Score && player1Score >= 3)
                return "Deuce";
            return string.Format("{0}-{1}", ScoreStrings[player1Score], ScoreStrings[player2Score]);
        }
        public void PlayerOneScores()
        {
            player1Score++;
        }

        public void PlayerTwoScores()
        {
            player2Score++;
        }
    }
}
