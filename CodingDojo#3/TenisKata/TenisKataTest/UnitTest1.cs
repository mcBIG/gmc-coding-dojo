﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TenisKataTest
{
    [TestClass]
    public class UnitTest1
    {
        Game game;

        [TestInitialize]
        public void InitialiseGame()
        {
            game = new Game();
        }

        public void InitializeScoreForPlayer(int player1Score, int player2Score) 
        {
            for (int i = 0; i < player1Score; i++)
            {
                game.PlayerOneScores();
            }

            for (int i = 0; i < player2Score; i++)
            {
                game.PlayerTwoScores();
            }
        }

        [TestMethod]
        public void TestGetScore_newGame_LoveLove()
        {
            var result = game.GetScore();
            Assert.AreEqual("Love-Love", result);
        }
        [TestMethod]
        public void TestGetScore_Player1ScoreIs1_FifteenLove()
        {
            game.PlayerOneScores();
            var result = game.GetScore();
            Assert.AreEqual("Fifteen-Love", result);
        }
        [TestMethod]
        public void TestGetScore_BothPlayersScoreIs1_FifteenFifteen()
        {
            InitializeScoreForPlayer(1, 1);
            var result = game.GetScore();
            Assert.AreEqual("Fifteen-Fifteen", result);
        }

        [TestMethod]
        public void TestGetScore_PlayerOneScore2PlayerTwoScore0_ThirtyLove()
        {
            InitializeScoreForPlayer(2, 0);
            var result = game.GetScore();
            Assert.AreEqual("Thirty-Love", result);
        }

        [TestMethod]
        public void TestGetScore_PlayerOneScore3PlayerTwoScore0_FourtyLove()
        {
            game.PlayerOneScores();
            game.PlayerOneScores();
            game.PlayerOneScores();
            var result = game.GetScore();
            Assert.AreEqual("Fourty-Love", result);
        }

        [TestMethod]
        public void TestGetScore_PlayerOneScore4PlayerTwoScore4_ExpectedDeuce()
        {
            InitializeScoreForPlayer(4, 4);
            var result = game.GetScore();
            Assert.AreEqual("Deuce", result);
        }


    }
}
