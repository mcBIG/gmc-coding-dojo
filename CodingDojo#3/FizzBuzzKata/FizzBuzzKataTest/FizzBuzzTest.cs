﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzzKata;

namespace FizzBuzzKataTest
{
    [TestClass]
    public class FizzBuzzTest
    {
        [TestMethod]
        public void TestOfNumber1_ShouldReturn1()
        {
            Assert.AreEqual("1", FizzBuzz.GetStringOf(1));
        }
        [TestMethod]
        public void TestOfNumber2_ShouldReturn2()
        {
            Assert.AreEqual("2", FizzBuzz.GetStringOf(2));
        }

        [TestMethod]
        public void TestOfNumber3_ShouldReturnFizz()
        {
            Assert.AreEqual("Fizz", FizzBuzz.GetStringOf(3));
        }

        [TestMethod]
        public void TestOfNumber6_ShouldReturnFizz()
        {
            Assert.AreEqual("Fizz", FizzBuzz.GetStringOf(6));
        }

        [TestMethod]
        public void TestOfNumber5_ShouldReturnBuzz()
        {
            Assert.AreEqual("Buzz", FizzBuzz.GetStringOf(5));
        }


    }
}
