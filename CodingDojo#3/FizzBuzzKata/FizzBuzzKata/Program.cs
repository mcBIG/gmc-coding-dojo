﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzKata
{
    public class FizzBuzz
    {
        static void Main(string[] args)
        {
        }

        public static string GetStringOf(int input)
        {
            if ((input % 3) == 0)
                return "Fizz";

            return input.ToString();
        }
    }
}
