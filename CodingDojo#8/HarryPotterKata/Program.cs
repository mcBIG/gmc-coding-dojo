﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1
{
    [TestClass]
    public class MyTestClass
    {
        public Cart cart;
        [TestInitialize]
        public void init()
        {
            cart = new Cart();
        }

        [TestMethod]
        public void Sum_AddFirstBookOnce_ShouldBe8()
        {

            cart.Add(Books.First);
            Assert.AreEqual(8, cart.GetTotalPrice());
        }

        [TestMethod]
        public void Sum_AddFirstBookTwice_ShouldBe16()
        {
            cart.Add(Books.First);
            cart.Add(Books.First);

            Assert.AreEqual(16, cart.GetTotalPrice());
        }

        [TestMethod]
        public void Sum_AddFirstBookOnceSecondOnce_ShouldBe15_2()
        {
            cart.Add(Books.First);
            cart.Add(Books.Second);

            Assert.AreEqual(15.2, cart.GetTotalPrice());
        }
        [TestMethod]
        public void Sum_AddFirstBookOnceSecondTwice_ShouldBe23_2()
        {
            cart.Add(Books.First);
            cart.Add(Books.Second);
            cart.Add(Books.Second);

            Assert.AreEqual(23.2, cart.GetTotalPrice());
        }

        [TestMethod]
        public void Sum_AddFirstAndSecondBookTwice_ShouldBe30_4()
        {
            cart.Add(Books.First);
            cart.Add(Books.First);
            cart.Add(Books.Second);
            cart.Add(Books.Second);

            Assert.AreEqual(30.4, cart.GetTotalPrice());
        }

        [TestMethod]
        public void Sum_AddFirstSecondAndThirdBookOnce_ShouldBe21_6()
        {
            cart.Add(Books.First);
            cart.Add(Books.Second);
            cart.Add(Books.Third);

            Assert.AreEqual(21.6, cart.GetTotalPrice());
        }

        [TestMethod]
        public void Sum_AddFirstSecondThirdAndFourthBookOnce_ShouldBe25_6()
        {
            cart.Add(Books.First);
            cart.Add(Books.Second);
            cart.Add(Books.Third);
            cart.Add(Books.Fourth);

            Assert.AreEqual(25.6, cart.GetTotalPrice());
        }

        [TestMethod]
        public void Sum_EmptyCart_ShouldReturn0()
        {
            Assert.AreEqual(0, cart.GetTotalPrice());
        }

        [TestMethod]
        public void Sum_AddFirstSecondThirdAndFourthBookTwice_ShouldBe51_2()
        {
            cart.Add(Books.First);
            cart.Add(Books.Second);
            cart.Add(Books.Third);
            cart.Add(Books.Fourth);
            cart.Add(Books.First);
            cart.Add(Books.Second);
            cart.Add(Books.Third);
            cart.Add(Books.Fourth);

            Assert.AreEqual(51.2, cart.GetTotalPrice());
        }
    }

    public class Cart
    {
        Dictionary<Books, int> Content = new Dictionary<Books, int>() {
            { Books.First, 0}, { Books.Second, 0}, { Books.Third, 0 }, { Books.Fourth, 0 }
        };

        Dictionary<int, double> Discount = new Dictionary<int, double>()
        {
            {0,1}, {1,1}, {2,.95}, {3,.9}, {4,.8}
        };

        public void Add(Books bookType)
        {
            Content[bookType]++;
        }

        public double GetTotalPrice()
        {
       
            double sum = 0;
            int iter = 0;
            Dictionary<Books, int> uniqueBooks;

            do
            {
                uniqueBooks = Content.Where(pair => pair.Value > iter)
                                     .ToDictionary(p => p.Key, p => p.Value);
                sum += uniqueBooks.Count * 8 * GetDiscountForUniqueBooks(uniqueBooks.Count);
                iter++;
            } while (uniqueBooks.Any());

            return sum;
        }
        
        private double GetDiscountForUniqueBooks(int uniqueBooksCount)
        {
            return Discount[uniqueBooksCount];
        }
    }

    public enum Books
    {
        First,
        Second,
        Third,
        Fourth
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
