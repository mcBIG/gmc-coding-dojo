﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TenisKataTest
{
    public static class ScoreBoard
    {

        static string[] scoreNames = new string[] { "Love", "Fifteen", "Thirty", "Fourty" };

        public static string GetScore(int pointOfPlayer1, int pointOfPlayer2)
        {
            int scoreDifference = pointOfPlayer1 - pointOfPlayer2;
            if (pointOfPlayer1 < 3 || pointOfPlayer2 < 3)
                return String.Format("{0}:{1}", scoreNames[pointOfPlayer1], scoreNames[pointOfPlayer2]);
            if (Math.Abs(scoreDifference) >= 2)
                return (scoreDifference > 0) ? "Player 1 wins" : "Player 2 wins";
            if (pointOfPlayer1 == pointOfPlayer2)
                return "Deuce";
            if (pointOfPlayer1 > pointOfPlayer2)
                return "Advantage Player 1";
            return "Advantage Player 2";
        }
    }

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetScore_WithScoreZeroZero_ReturnsLoveLove()
        {
            Assert.AreEqual("Love:Love", ScoreBoard.GetScore(0, 0));
        }

        [TestMethod]
        public void GetScore_WithScoreOneZero_ReturnsFifteenLove()
        {
            Assert.AreEqual("Fifteen:Love", ScoreBoard.GetScore(1, 0));
        }
        [TestMethod]
        public void GetScore_WithScoreZeroOne_ReturnsLoveFifteen()
        {
            Assert.AreEqual("Love:Fifteen", ScoreBoard.GetScore(0, 1));
        }

        [TestMethod]
        public void GetScore_WithScoreTwoZero_ReturnsThirtyLove()
        {
            Assert.AreEqual("Thirty:Love", ScoreBoard.GetScore(2, 0));
        }

        [TestMethod]
        public void GetScore_WithScoreTwoThree_ReturnsThirtyFourty()
        {
            Assert.AreEqual("Thirty:Fourty", ScoreBoard.GetScore(2, 3));
        }

        [TestMethod]
        public void GetScore_WithScoreThreeThree_ReturnsDeuce()
        {
            Assert.AreEqual("Deuce", ScoreBoard.GetScore(3, 3));
        }

        [TestMethod]
        public void GetScore_WithScoreThreeFour_ReturnsAdvantagePlayer2()
        {
            Assert.AreEqual("Advantage Player 2", ScoreBoard.GetScore(3, 4));
        }

        [TestMethod]
        public void GetScore_WithScoreFourThree_ReturnsAdvantagePlayer1()
        {
            Assert.AreEqual("Advantage Player 1", ScoreBoard.GetScore(4, 3));
        }

        [TestMethod]
        public void GetScore_WithScoreFiveFour_ReturnsAdvantagePlayer1()
        {
            Assert.AreEqual("Advantage Player 1", ScoreBoard.GetScore(5, 4));
        }

        [TestMethod]
        public void GetScore_WithScoreSevenFive_ReturnsPlayer1Wins()
        {
            Assert.AreEqual("Player 1 wins", ScoreBoard.GetScore(7, 5));
        }
        [TestMethod]
        public void GetScore_WithScoreFiveSeven_ReturnsPlayer2Wins()
        {
            Assert.AreEqual("Player 2 wins", ScoreBoard.GetScore(5, 7));
        }

        [TestMethod]
        public void Player1Scores_AddPointToPlayer1_Player1ShouldHavePoint()
        {
            Referee referee = new Referee();
            referee.Player1Scores();
            Assert.AreEqual(1, referee.Player1Score);
        }
    }
}
